# coding: utf-8

def transliterate(str):
    pairs = {
      'á': 'a',
      'é': 'e',
      'ó': 'o',
      'ö': 'o',
      'ő': 'ő',
      'ú': 'u',
      'ü': 'u',
      'ű': 'u',
    }
    for fr, to in pairs.items():
        str = str.replace(fr, to)

    return str

def to_dot_line(connection):
    return transliterate( '"' + connection[0] + '" -> "' + connection[1] + '"' )

def to_dot(calculated_nodes, direct_connections, mq_connections, connections_to_mq):
    dot = "digraph network {\n"

    for node in calculated_nodes.values():
        name = transliterate(node['name'])
        oa = node['own_availability'] * 100
        da = node['derived_availability'] * 100
        uses_t = len(node['uses_transitive'])
        serves_t = len(node['serves_transitive'])

        dot += '  "' + name + '" [shape=box style=filled'

        label_format = 'label="'\
                       '{}\l' \
                       'own avail: {:.4f}%\l' \
                       'derived avail: {:.4f}%\l' \
                       'uses transitive: {}\l' \
                       'serves transitive: {}\l' \
                       '"'
        label = label_format.format(name, oa, da, uses_t, serves_t)
        dot += ' ' + label

        if node['pos_x'] and node['pos_y']:
            x = float(node['pos_x'])  / 50
            y = -float(node['pos_y']) / 50
            position = 'pos="{},{}!"'.format(x, y)
            dot += ' ' + position

        if node['derived_availability'] < .97:
            color = 'fillcolor="#ffa4a4"'
        elif node['derived_availability'] < .995:
            color = 'fillcolor="#ffd3a4"'
        elif node['derived_availability'] < .9995:
                color = 'fillcolor="#feffd4"'
        else:
            color = 'fillcolor=white'

        dot += ' ' + color

        dot += ']' + "\n"

    dot += "\n"

    for connection in direct_connections:
        dot += '  ' + to_dot_line(connection) + ' [style=solid]' + "\n"
    for connection in mq_connections:
        dot += '  ' + to_dot_line(connection) + ' [style=dashed]' + "\n"
    for connection in connections_to_mq:
        dot += '  ' + to_dot_line(connection) + ' [style=solid, color=grey]' + "\n"

    dot += "}\n"
    return dot
