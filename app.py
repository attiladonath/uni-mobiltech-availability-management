# coding: utf-8

from graph import *
from utils import to_dot

nodes = {}
duplicated_nodes = [
    # Alkalmazásszerver
    # "Netcon Alkalmazásszerver",
    # "Számlázási alkalmazásszerver",
    # "SAP alkalmazásszerver",
    # "Custpack alkalmazásszerver",
    # "Staffman Alkalmazásszerver",
    # "Pénztári alkalmazásszerver / Citrix",
    # "Know-IT-All alkalmazásszerver",
    # "Netline Alkalmazásszerver",

    # Adatbázisszerver
    # "Netcon Adatbázisszerver",
    # "Számlázási adatbázisszerver",
    # "SAP adatbázis szerver",
    # "Custpack adatbázisszerver",
    # "Pénztári adatbázis szerver",
    # "Perso Adatbázisszerver",
    # "Staffman adatbázisszerver",
    # "Know-IT-All adatbázisszerver",
    # "Netline Adatbázisszerver",

    # Fizikai
    # "Perso és Staffman szerver",
    # "Netcon szerver",
    # "SAP szerver",
    # "Számlázás és Pénztár cluster",
    # "Custpack, Netline, Know-IT-All cluster",
]

for name, downtime in network_nodes.items():
    if name in duplicated_nodes:
        downtime = downtime * downtime

    nodes[name] = {
        'name': name,
        'own_downtime': downtime,
        'own_availability': 1 - downtime,
        'derived_availability': None,
        'pos_x': None,
        'pos_y': None,
        'uses': [],
        'uses_transitive': None,
        'serves': [],
        'serves_transitive': None,
    }

for name, pos in network_nodes_position.items():
    nodes[name]['pos_x'] = pos['x']
    nodes[name]['pos_y'] = pos['y']

for fr, to in all_connections:
    nodes[fr]['uses'].append(to)
    nodes[to]['serves'].append(fr)

###############

def connection_transitive(name, type):
    node_list = []
    for connected_name in nodes[name][type]:
        node_list += [connected_name] + connection_transitive(connected_name, type)

    # Remove duplicates
    return list(set(node_list))

###############

for name in nodes:
    nodes[name]['uses_transitive'] = connection_transitive(name, 'uses')
    nodes[name]['serves_transitive'] = connection_transitive(name, 'serves')

    nodes[name]['derived_availability'] = nodes[name]['own_availability']
    for used_name in nodes[name]['uses_transitive']:
        nodes[name]['derived_availability'] *= nodes[used_name]['own_availability']

print( to_dot(nodes, direct_connections, mq_connections, connections_to_mq) )
