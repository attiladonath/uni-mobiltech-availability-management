# Requirements

Python 2 or Python 3

Tested with:
- Python 2.7.12
- Python 3.5.2

# Usage

## Output to DOT (Graphviz)

```
python app.py > calculated.dot
```

## Output to SVG

```
python app.py | neato -Tsvg > calculated.svg
```

## Display DOT in the browser

Use [Viz.js](http://viz-js.com/).

Engine: neato
Format: svg

![Viz.js in the browser](viz_js.png)

# Configuration

Uncomment lines in [app.py](app.py) in the `duplicated_nodes` list to duplicate a node.
